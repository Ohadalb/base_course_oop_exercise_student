package Missions;

import Abilities.IAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

import java.util.HashMap;

public abstract class Mission {

  private Coordinates destination;
  private String pilotName;
  private AerialVehicle aerialVehicle;
  private HashMap<String, IAbility> abilities;

  public Mission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle) {
    this.setDestination(destination);
    this.setPilotName(pilotName);
    this.setAerialVehicle(aerialVehicle);
    this.setAbilities(new HashMap<>());
  }

  public HashMap<String, IAbility> getAbilities() {
    return this.abilities;
  }

  public void setAbilities(HashMap<String, IAbility> abilities) {
    this.abilities = abilities;
  }

  public void addAbility(IAbility ability) {
    this.getAbilities().put(ability.getClass().getSimpleName(), ability);
  }

  public Coordinates getDestination() {
    return this.destination;
  }

  public void setDestination(Coordinates destination) {
    this.destination = destination;
  }

  public String getPilotName() {
    return this.pilotName;
  }

  public void setPilotName(String pilotName) {
    this.pilotName = pilotName;
  }

  public AerialVehicle getAerialVehicle() {
    return this.aerialVehicle;
  }

  public void setAerialVehicle(AerialVehicle aerialVehicle) {
    this.aerialVehicle = aerialVehicle;
  }

  public abstract String executeMission();

  public void begin() {
    System.out.println("beginning Mission!");
    this.aerialVehicle.flyTo(this.getDestination());
  }

  public void cancel() {
    System.out.println("Abort Mission");
    this.aerialVehicle.land(this.getDestination());
  }

  public void finish() {
    System.out.println(this.executeMission());
    this.aerialVehicle.land(this.destination);
    System.out.println("Finish Mission");
  }

  //	public Ability extractForMission(String abilityName) {
  //		Ability matchAbility = this.getAerialVehicle().getAbilities().get(0);
  //		for (int i = 0; i < this.getAerialVehicle().getAbilities().size(); i++) {
  //			if (this.getAerialVehicle().getAbilities().get(i).getAbilityName().equals(abilityName))
  //				matchAbility = this.getAerialVehicle().getAbilities().get(i);
  //		}
  //		return matchAbility;
  //	}
}
