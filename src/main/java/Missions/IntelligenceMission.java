package Missions;

import Abilities.IAbility;
import Abilities.IntelligenceAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
  private String region;

  public IntelligenceMission(
      Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String region)
      throws AerialVehicleNotCompatibleException {
    super(destination, pilotName, aerialVehicle);
    this.setRegion(region);
    checkAerialVehicle(aerialVehicle);
  }

  public String getRegion() {
    return this.region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public void checkAerialVehicle(AerialVehicle aerialVehicle)
      throws AerialVehicleNotCompatibleException {
    boolean goodAerialVehicle = false;
    for (IAbility currAbility : aerialVehicle.getAbilities()) {
      if (currAbility instanceof IntelligenceAbility) {
        goodAerialVehicle = !goodAerialVehicle;
      }
      this.addAbility(currAbility);
    }
    if (!goodAerialVehicle) {
      throw new AerialVehicleNotCompatibleException(
          "The aerial vehicle isnt compatiable to the mission");
    }
  }

  @Override
  public String executeMission() {
    IntelligenceAbility intelligenceAbility =
        (IntelligenceAbility) this.getAbilities().get("IntelligenceAbility");
    return this.getPilotName()
        + ":"
        + this.getAerialVehicle().getClass().getSimpleName()
        + "Collecting Data in "
        + this.getRegion()
        + "with: sensor type: "
        + intelligenceAbility.getSensorType().name();
  }
}
