package Missions;

import Abilities.AttackAbility;
import Abilities.IAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class AttackMission extends Mission {
  private String target;

  public AttackMission(
      String target, Coordinates destination, String pilotName, AerialVehicle aerialVehicle)
      throws AerialVehicleNotCompatibleException {
    super(destination, pilotName, aerialVehicle);
    this.setTarget(target);
    checkAerialVehicle(aerialVehicle);
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public void checkAerialVehicle(AerialVehicle aerialVehicle)
      throws AerialVehicleNotCompatibleException {
    boolean goodAerialVehicle = false;
    for (IAbility currAbility : aerialVehicle.getAbilities()) {
      if (currAbility instanceof AttackAbility) {
        goodAerialVehicle = !goodAerialVehicle;
      }
      this.addAbility(currAbility);
    }
    if (!goodAerialVehicle) {
      throw new AerialVehicleNotCompatibleException(
          "The aerial vehicle isnt compatiable to the mission");
    }
  }

  @Override
  public String executeMission() {
    AttackAbility attackAbility = (AttackAbility) this.getAbilities().get("AttackAbility");
    return this.getPilotName()
        + ":"
        + this.getAerialVehicle().getClass().getSimpleName()
        + "Attacking "
        + this.getTarget()
        + " with: "
        + attackAbility.getRocketsTypes().name()
        + "X"
        + attackAbility.getRocketsAmount();
  }
}
