package Missions;

import Abilities.BdaAbility;
import Abilities.IAbility;
import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public class BdaMission extends Mission {

  private String objective;

  public BdaMission(
      Coordinates destination, String pilotName, AerialVehicle aerialVehicle, String objective)
      throws AerialVehicleNotCompatibleException {
    super(destination, pilotName, aerialVehicle);
    this.setObjective(objective);
    checkAerialVehicle(aerialVehicle);
  }

  public String getObjective() {
    return this.objective;
  }

  public void setObjective(String objective) {
    this.objective = objective;
  }

  public void checkAerialVehicle(AerialVehicle aerialVehicle)
      throws AerialVehicleNotCompatibleException {
    boolean goodAerialVehicle = false;
    for (IAbility currAbility : aerialVehicle.getAbilities()) {
      if (currAbility instanceof BdaAbility) {
        goodAerialVehicle = !goodAerialVehicle;
      }
      this.addAbility(currAbility);
    }
    if (!goodAerialVehicle) {
      throw new AerialVehicleNotCompatibleException(
          "The aerial vehicle isnt compatiable to the mission");
    }
  }

  @Override
  public String executeMission() {
    BdaAbility bdaAbility = (BdaAbility) this.getAbilities().get("BdaAbility");
    return this.getPilotName()
        + ":"
        + this.getAerialVehicle().getClass().getSimpleName()
        + "talking pictures of "
        + this.getObjective()
        + " with: "
        + bdaAbility.getCameraType().name();
  }
}
