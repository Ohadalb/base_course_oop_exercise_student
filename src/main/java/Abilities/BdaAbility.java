package Abilities;

import Enums.CamerasTypes;

public class BdaAbility implements IAbility {
  CamerasTypes CameraType;

  public BdaAbility(CamerasTypes CameraType) {
    this.setCameraType(CameraType);
  }

  public CamerasTypes getCameraType() {
    return this.CameraType;
  }

  public void setCameraType(Enums.CamerasTypes cameraType) {
    this.CameraType = cameraType;
  }
}
