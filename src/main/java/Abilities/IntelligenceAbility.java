package Abilities;

import Enums.SensorsTypes;

public class IntelligenceAbility implements IAbility {
  SensorsTypes SensorsTypes;

  public IntelligenceAbility(SensorsTypes cameraTypes) {
    this.setSensorsTypes(cameraTypes);
  }

  public SensorsTypes getSensorType() {
    return this.SensorsTypes;
  }

  public void setSensorsTypes(Enums.SensorsTypes sensorsTypes) {
    this.SensorsTypes = sensorsTypes;
  }
}
