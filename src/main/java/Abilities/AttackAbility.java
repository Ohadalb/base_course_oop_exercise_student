package Abilities;

import Enums.RocketsTypes;

public class AttackAbility implements IAbility {

  private int rocketsAmount;
  private RocketsTypes RocketsTypes;

  public AttackAbility(int rocketsAmount, RocketsTypes rocketstypes) {
    this.setRocketsAmount(rocketsAmount);
    this.setRocketsTypes(rocketstypes);
  }

  public int getRocketsAmount() {
    return this.rocketsAmount;
  }

  public void setRocketsAmount(int rocketsAmount) {
    this.rocketsAmount = rocketsAmount;
  }

  public RocketsTypes getRocketsTypes() {
    return this.RocketsTypes;
  }

  public void setRocketsTypes(Enums.RocketsTypes rocketsTypes) {
    this.RocketsTypes = rocketsTypes;
  }
}
