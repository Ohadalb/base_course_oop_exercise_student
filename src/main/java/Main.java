import Abilities.AttackAbility;
import Abilities.BdaAbility;
import Abilities.IntelligenceAbility;
import AerialVehicles.F15;
import AerialVehicles.Shoval;
import Entities.Coordinates;
import Enums.AirplaneStatus;
import Enums.CamerasTypes;
import Enums.RocketsTypes;
import Enums.SensorsTypes;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;

public class Main {

  public static void main(String[] args) {

    F15 f15 = new F15(AirplaneStatus.READY_TO_FLIGHT, new Coordinates(25.0, 50.0));
    f15.addAbility(new AttackAbility(20, RocketsTypes.PYTHON));
    f15.addAbility(new IntelligenceAbility(SensorsTypes.ELINT));

    try {
      AttackMission attackMission =
          new AttackMission("zriphin", new Coordinates(75.0, 75.0), "gibor", f15);
      attackMission.begin();
      attackMission.finish();
    } catch (AerialVehicleNotCompatibleException e) {
      e.printStackTrace();
    }

    System.out.println(
        "-------------------------------------------------------------------------------------");
    try {
      IntelligenceMission intelligenceMission =
          new IntelligenceMission(new Coordinates(100.0, 100.0), "yosi", f15, "yehud");
      intelligenceMission.begin();
      intelligenceMission.finish();

      System.out.println(
          "-------------------------------------------------------------------------------------");

      intelligenceMission.begin();
      intelligenceMission.cancel();
    } catch (AerialVehicleNotCompatibleException e) {
      e.printStackTrace();
    }

    System.out.println(
        "-------------------------------------------------------------------------------------");

    Shoval shoval = new Shoval(50, AirplaneStatus.READY_TO_FLIGHT, new Coordinates(400.0, 400.0));
    shoval.addAbility(new IntelligenceAbility(SensorsTypes.INFRA_RED));
    shoval.addAbility(new AttackAbility(3, RocketsTypes.AMRAM));
    shoval.addAbility(new BdaAbility(CamerasTypes.THERMAL));

    shoval.hoverOverLocation(new Coordinates(500.0, 500.0));

    try {
      AttackMission attackMissionShoval =
          new AttackMission("GAZA", new Coordinates(3000.0, 3000.0), "OHAD", shoval);
      attackMissionShoval.begin();
      attackMissionShoval.finish();
    } catch (AerialVehicleNotCompatibleException e) {
      e.printStackTrace();
    }

    System.out.println(
        "-------------------------------------------------------------------------------------");

    try {
      IntelligenceMission intelligenceMissionShoval =
          new IntelligenceMission(new Coordinates(700.0, 700.0), "yahav", shoval, "labnon");
      intelligenceMissionShoval.begin();
      intelligenceMissionShoval.finish();

    } catch (AerialVehicleNotCompatibleException e) {
      e.printStackTrace();
    }

    System.out.println(
        "-------------------------------------------------------------------------------------");

    try {
      BdaMission bdaMission =
          new BdaMission(new Coordinates(700.0, 700.0), "moshe", shoval, "labnon");
      bdaMission.begin();
      bdaMission.cancel();
    } catch (AerialVehicleNotCompatibleException e) {
      e.printStackTrace();
    }
  }
}
