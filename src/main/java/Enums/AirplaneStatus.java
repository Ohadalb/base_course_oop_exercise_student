package Enums;

public enum AirplaneStatus {
  READY_TO_FLIGHT,
  IN_AIR,
  NOT_READY_TO_FLIGHT
}
