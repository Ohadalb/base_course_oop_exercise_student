package Enums;

public enum CamerasTypes {
  REGULAR,
  THERMAL,
  NIGHT_VISION
}
