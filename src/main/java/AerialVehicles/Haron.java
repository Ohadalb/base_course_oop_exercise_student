package AerialVehicles;

import Entities.Coordinates;
import Enums.AirplaneStatus;

public abstract class Haron extends Drone {
  static final int TREATMENT_HOURS = 150;

  public Haron(int flightHours, AirplaneStatus flightStatues, Coordinates homeBase) {
    super(TREATMENT_HOURS, flightHours, flightStatues, homeBase);
  }
}
