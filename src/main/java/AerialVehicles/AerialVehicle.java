package AerialVehicles;

import Abilities.IAbility;
import Entities.Coordinates;
import Enums.AirplaneStatus;

import java.util.ArrayList;
import java.util.List;

public abstract class AerialVehicle {

  private int timeTreatment;
  private int flightHours;
  private AirplaneStatus airplaneStatus;
  private Coordinates homeBase;
  private ArrayList<IAbility> abilities;

  public AerialVehicle(
      int timeTreatment, int flightHours, AirplaneStatus airplaneStatus, Coordinates homeBase) {
    this.setTimeTreatment(timeTreatment);
    this.setFlightHours(flightHours);
    this.setAirplaneStatus(airplaneStatus);
    this.setHomeBase(homeBase);
    this.setAbilities(new ArrayList<IAbility>());
  }

  public int getTimeTreatment() {
    return this.timeTreatment;
  }

  public void setTimeTreatment(int timeTreatment) {
    this.timeTreatment = timeTreatment;
  }

  public int getFlightHours() {
    return this.flightHours;
  }

  public void setFlightHours(int flightHours) {
    this.flightHours = flightHours;
  }

  public AirplaneStatus getAirplaneStatus() {
    return this.airplaneStatus;
  }

  public void setAirplaneStatus(AirplaneStatus airplaneStatus) {
    this.airplaneStatus = airplaneStatus;
  }

  public Coordinates getHomeBase() {
    return this.homeBase;
  }

  public void setHomeBase(Coordinates homeBase) {
    this.homeBase = homeBase;
  }

  public List<IAbility> getAbilities() {
    return this.abilities;
  }

  public void setAbilities(ArrayList<IAbility> abilities) {
    this.abilities = abilities;
  }

  public void addAbility(IAbility ability) {
    this.getAbilities().add(ability);
  }

  public void flyTo(Coordinates destination) {
    if (this.airplaneStatus == AirplaneStatus.NOT_READY_TO_FLIGHT) {
      System.out.println("Aerial Vehicle isn't ready to fly");
    } else {
      System.out.println("flying to " + destination.toString());
      this.airplaneStatus = AirplaneStatus.IN_AIR;
    }
  }

  public void land(Coordinates coordinates) {
    System.out.println("landing on " + coordinates.toString());
    this.check();
  }

  public void check() {
    if (this.flightHours >= this.timeTreatment) {
      this.airplaneStatus = AirplaneStatus.NOT_READY_TO_FLIGHT;
      this.repair();
    } else {
      this.airplaneStatus = AirplaneStatus.READY_TO_FLIGHT;
    }
  }

  public void repair() {
    this.flightHours = 0;
    this.airplaneStatus = AirplaneStatus.READY_TO_FLIGHT;
  }
}
