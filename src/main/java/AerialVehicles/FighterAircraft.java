package AerialVehicles;

import Entities.Coordinates;
import Enums.AirplaneStatus;

public abstract class FighterAircraft extends AerialVehicle {
  static final int TREATMENT_HOURS = 250;
  static final int NEW_FighterAircraft = 0;

  public FighterAircraft(int flightHours, AirplaneStatus airplaneStatus, Coordinates homeBase) {
    super(TREATMENT_HOURS, flightHours, airplaneStatus, homeBase);
  }

  public FighterAircraft(AirplaneStatus airplaneStatus, Coordinates homeBase) {
    super(TREATMENT_HOURS, NEW_FighterAircraft, airplaneStatus, homeBase);
  }
}
