package AerialVehicles;

import Entities.Coordinates;
import Enums.AirplaneStatus;

public abstract class Drone extends AerialVehicle {
  public Drone(
      int timeTreatment, int flightHours, AirplaneStatus flightStatues, Coordinates homeBase) {
    super(timeTreatment, flightHours, flightStatues, homeBase);
  }

  public void hoverOverLocation(Coordinates destination) {
    System.out.println("Hovering Over: " + destination.toString());
  }
}
