package AerialVehicles;

import Entities.Coordinates;
import Enums.AirplaneStatus;

public abstract class Hermes extends Drone {
  static final int TREATMENT_HOURS = 150;

  public Hermes(int flightHours, AirplaneStatus flightStatues, Coordinates homeBase) {
    super(100, flightHours, flightStatues, homeBase);
  }
}
