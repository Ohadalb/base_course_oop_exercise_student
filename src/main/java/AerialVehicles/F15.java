package AerialVehicles;

import Entities.Coordinates;
import Enums.AirplaneStatus;

public class F15 extends FighterAircraft {

  public F15(int flightHours, AirplaneStatus airplaneStatus, Coordinates homeBase) {
    super(flightHours, airplaneStatus, homeBase);
  }

  public F15(AirplaneStatus airplaneStatus, Coordinates homeBase) {
    super(airplaneStatus, homeBase);
  }
}
