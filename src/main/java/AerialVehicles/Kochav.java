package AerialVehicles;

import Entities.Coordinates;
import Enums.AirplaneStatus;

public class Kochav extends Hermes {

  public Kochav(int flightHours, AirplaneStatus flightStatues, Coordinates homeBase) {
    super(flightHours, flightStatues, homeBase);
  }
}
