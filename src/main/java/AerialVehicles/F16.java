package AerialVehicles;

import Entities.Coordinates;
import Enums.AirplaneStatus;

public class F16 extends FighterAircraft {
  public F16(int flightHours, AirplaneStatus airplaneStatus, Coordinates homeBase) {
    super(flightHours, airplaneStatus, homeBase);
  }

  public F16(AirplaneStatus airplaneStatus, Coordinates homeBase) {
    super(airplaneStatus, homeBase);
  }
}
